# Terraform Automation for Managing AWS Resources

This Terraform script provides automation for managing AWS resources using infrastructure as code techniques. With this script, you can configure AWS Route 53, Load Balancer, EC2 Instances, and RDS database easily and efficiently.

## Project Structure

This script uses a modular structure to manage AWS resources, with two modules named `production` and `development`. The `production` module is for the production environment, while the `development` module is for the development environment.

## Workflow

The GitLab CI/CD workflow for this script consists of four stages:

1. Validate: validate the Terraform configuration syntax.
2. Plan: execute `terraform plan` to show what changes will be applied.
3. Apply: apply the changes to AWS using `terraform apply`.
4. Destroy: destroy the resources when needed using `terraform destroy`.

## Variables

To use this script, you need to set some environment variables, such as:

* `AWS_ACCESS_KEY_ID`: the access key for accessing AWS.
* `AWS_SECRET_ACCESS_KEY`: the secret key for accessing AWS.
* `GITLAB_ACCESS_TOKEN`: the GitLab access token for storing the Terraform state file.

## Stages

* `validate`: validate the Terraform configuration syntax.
* `plan`: show what changes will be applied.
* `apply`: apply the changes to AWS.
* `destroy`: destroy the resources on AWS.

## Usage

To use this Terraform script, follow these steps:

1. Set the necessary environment variables.
2. Create a new GitLab project, and configure a new state backend for Terraform as GitLab HTTP.
3. Update the `TF_DIR`, `STATE_NAME`, and `ADDRESS` in `.gitlab-ci.yml` file with your own values.
4. Upload the Terraform configuration files to the `terraform/production` and `terraform/development` directories.
5. Commit and push the changes, and the GitLab CI/CD pipeline will be triggered automatically.

Note: Be sure to set the appropriate permissions for the GitLab runner to access and modify the AWS resources.

## Conclusion

This Terraform script provides an efficient and reliable way to manage AWS resources using infrastructure as code practices. With this script, you can configure and maintain your AWS resources easily, improving the reliability and consistency of your infrastructure.

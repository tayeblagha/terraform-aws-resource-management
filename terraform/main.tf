terraform {


  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  // manage state file backen in gitlab
  backend "http" { }
  
}

provider "aws" {
  region = "us-east-1"
}

// Module n°1 

module "web_app_1" {
  source = "./terraform-module"

  # Input Variables
  bucket_prefix    = "web-app-1-data"
  domain           = "devopsdeployed.com"
  app_name         = "web-app-1"
  environment_name = "production"
  instance_type    = "t2.micro"
  create_dns_zone  = true
  db_name          = "webapp1db"
  db_user          = "foo"
  db_pass          = var.db_pass_1
}
// Module n°2 

module "web_app_2" {
  source = "./terraform-module"

  # Input Variables
  bucket_prefix    = "web-app-2-data"
  domain           = "anotherdevopsdeployed.com"
  app_name         = "web-app-2"
  environment_name = "development"
  instance_type    = "t2.micro"
  create_dns_zone  = true
  db_name          = "webapp2db"
  db_user          = "bar"
  db_pass          = var.db_pass_2
}

// Variable Declaration

variable "db_pass_1" {
  description = "password for database #1"
  type        = string
  sensitive   = true
  default ="root12345"
}

variable "db_pass_2" {
  description = "password for database #2"
  type        = string
  sensitive   = true
  default ="root12345"

}